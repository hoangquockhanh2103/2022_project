const express = require('express');  
const route = express.Router();  

const siteController = require('../app/controllers/siteController');
//siteController-showhome

route.use('/',siteController.index); 


module.exports = route