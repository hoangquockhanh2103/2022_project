const { application } = require('express');
const express = require('express');  
const route = express.Router(); 

const newsController = require('../app/controllers/newsController');

//newsController.index 

route.use('/detail', newsController.detail);
route.use('/',newsController.index); 
 

module.exports = route;